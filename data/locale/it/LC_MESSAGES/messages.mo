��    O      �  k         �     �     �     �     �            	       #  z   +     �     �     �  �   �     �	     �	  T   �	      
  J   &
     q
     w
     �
     �
  3   �
  b   �
  l   +     �     �     �     �     �     �  
   �  5   �     )     .     6     ;     B  $   P     u     z  �   }     >  $   G  �   l       q   ,  j   �     	     )     /     ;     @     Y     _     f     k  
   w     �     �     �     �     �  %  �  �   �     o     x  #   �  *   �     �     �     
               6  !   P     r  1   �  �  �     F  "   [     ~  '   �  
   �     �  
   �  I  �  �   -     �  (   �  (   �  �     
   �     �  g        y  e   �     �     �            7     |   S  �   �     W     ^     {     �     �     �     �  ^   �     9     ?     G     N     V  1   i     �     �  �   �     r  2   ~  �   �     f   �   �   W   %!     }!     �!  
   �!     �!  $   �!  
   �!     �!     �!     �!     "     "     ""  #   +"     O"     h"  v  ~"  �   �&     �'  $   �'  $   �'  +   	(     5(     J(  	   d(  #   n(     �(     �(  ,   �(  "   �(  >    )     *   !   	                      L              4   H               7                        9   B   A      K             
   %   <   8      1   N   =   '             5      J   $   E   D   "      2         ,             ?       C                      3       ;   F   -   G              0   O      (   .       &   I      @           )      /         +                        #      :   6   >   M    About Additional aircraft path: Airport data source: Airport database update: Airport: Authors: Automatic Automatic - FGo! will try to keep track of changes of
    FG_ROOT/Airports/apt.dat.gz file, and will rebuild its airport database
    when need it.

Manual - "Rebuild Airport Database" button needs to be used every time
    FG_ROOT/Airports/apt.dat.gz is changed. Build new airport database from current FG_ROOT/Airports/apt.dat.gz.
Useful in case when apt.dat.gz file has been updated. Cancel Cannot find apt.dat database. Cannot read apt.dat database. Cannot read version number of apt.dat database.

FGo! expects a proper header information at the beginning of apt.dat. For details about data format see http://data.x-plane.com/designers.html#Formats Carrier: Change language: Choose other language. If not selected,
FGo! will try to choose the system language. Clear Click here to download the METAR report
for selected (or nearest) airport. Close Config Files Decoded Default Directory where TerraSync saves downloaded scenery. Enter the path to "fgfs" file, or "run_fgfs.sh", if you are using
download_and_compile.sh scripts. Enter the path to "terrasync" file, or "run_terrasync.sh", if you are using
download_and_compile.sh scripts. Error Fetching report... File Find FlightGear is running... FlightGear settings Font size: Generating airport database,
this can take a while... Help License Load Manual Miscellaneous Modification of apt.dat.gz detected. None OK Optional parameter specifying FlightGear's working directory.
That is the directory where logs or screen-shots will be saved. If left blank,
the working directory is the user's home directory. Parking: Path to FlightGear's main directory. Path to additional aircraft directory. Multiple directories separated by
a colon can be included. Leave it empty if you are not using additional
aircraft directories. Path to executable file: Path to scenery. You can specify more than one path (separated by a colon)
in order from highest to lowest layer. Please make sure that paths: FG_BIN and FG_ROOT
in "Preferences" window are pointing to right directories. Port address used by TerraSync. Port: Preferences Quit Rebuild Airport Database Reset Run FG Rwy: Save & Quit Save as... Save settings Scenery Scenery Prefetch Scenery path: Select Scenario Select data source where FGo! will be looking for information about runways or
parking positions. There are two options:

Default - FGo! will show runway numbers based on ~/.fgo/apt file,
    which is generated from FG_ROOT/Airports/apt.dat.gz database.
    Parking names will be taken from FG_ROOT/AI/Airports directory.

Scenery - FGo! will show runway numbers based on ~/.fgo/apt file,
    which is generated from FG_ROOT/Airports/apt.dat.gz database.
    Parking names will be taken from FG_SCENERY/Airports folder
    - or folders - if multiple scenery paths are provided.

Generally speaking: "Default" option will tell FGo! to show the same
data that FlightGear uses by default when selecting starting location,
while "Scenery" option will tell FGo! to look for that data
directly in scenery folder. In that latter case, you may add
--prop:/sim/paths/use-custom-scenery-data=true argument into command
line options window to tell FlightGear to use the same data.

If not sure which option you should choose, the best is to stick
with the default setting. Set the base font size in the range from {0} to {1}. Zero is a special
value that sets font to a platform-dependent default size. Settings Show installed airports only Starting %s with following options: Starting TerraSync with following command: Stopping TerraSync TerraSync settings Tools Translation: Unable to download data. Unable to run FlightGear! Update list of installed airports Working directory (optional): [FGo! Warning] Scenery prefetch was unsuccessful. Project-Id-Version: 1.5.4
POT-Creation-Date: 2014-11-06 18:44+CET
PO-Revision-Date: 2014-11-11 18:30+0200
Last-Translator: Philip Lacroix <philnx AT posteo DOT de>
Language-Team: 
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: ENCODING
Generated-By: pygettext.py 1.5
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.4
 Informazioni su FGo! Percorso aggiuntivo per gli aerei: Sorgente dati degli aeroporti: Aggiornamento database degli aeroporti: Aeroporto: Autori: Automatico Automatico - FGo! proverà a tenere conto delle modifiche apportate
  al file FG_ROOT/Airports/apt.dat.gz, ricostruendo quando necessario
  il database degli aeroporti.

Manuale - Ogni volta che il file FG_ROOT/Airports/apt.dat.gz è stato
  modificato bisogna utilizzare il pulsante "Ricostruisci il database
  degli aeroporti". Ricostruire il database degli aeroporti a partire dal file
FG_ROOT/Airports/apt.dat.gz corrente. Utile nel caso in
cui il file sia stato aggiornato. Annulla Impossibile trovare il database apt.dat. Impossibile leggere il database apt.dat. Impossibile leggere il numero di versione del database apt.dat.

FGo! si aspetta di trovare un'intestazione corretta all'inizio di apt.dat.Per dettagli sui formati vedi http://data.x-plane.com/designers.html#Formats Portaerei: Selezione lingua: Scegliere un'altra lingua. Se nessuna lingua è stata
selezionata verrà utilizzata quella del sistema. Cancella Fare clic qui per scaricare il rapporto METAR per
l'aeroporto selezionato (o per quello più vicino). Chiudi File di configurazione Decodificato Default Directory in cui TerraSync salva gli scenari scaricati. Inserire il percorso del file "fgfs", oppure di "run_fgfs.sh" se
si stanno utilizzando gli script "download_and_compile.sh". Inserire il percorso del file "terrasync", oppure di "run_terrasync.sh"
se si stanno utilizzando gli script "download_and_compile.sh". Errore Scaricamento del rapporto... File Cerca FlightGear è stato avviato... Impostazioni di FlightGear Dimensione del carattere: Generazione del database degli aeroporti in corso:
ciò potrebbe richiedere un po' di tempo... Aiuto Licenza Carica Manuale Altre impostazioni Sono state rilevate delle modifiche a apt.dat.gz. - OK Parametri facoltativi per specificare la directory di lavoro di FlightGear:
in questa directory verranno salvati i file di log e gli screenshot. Se il
campo è vuoto verrà utilizzata la directory dell'utente. Parcheggio: Percorso della directory principale di FlightGear. Percorso aggiuntivo per gli aerei. È possibile specificare più directory
separandole tra loro col carattere ":". Lasciare il campo vuoto se non
si utilizzano percorsi aggiuntivi. Percorso del file eseguibile: Percorso dello scenario. È possibile specificare più percorsi,
separandoli tra loro col carattere ":" e ordinandoli dal livello
più alto a quello più basso. Assicurarsi che i percorsi FG_BIN e FG_ROOT
nella finestra "Preferenze" siano corretti. Porta utilizzata da TerraSync. Porta: Preferenze Esci Ricostruire database degli aeroporti Ripristina Avvia FG Pista: Salva ed esci Salva come... Salva impostazioni Scenario Scarica preventivamente lo scenario Percorso dello scenario: Seleziona situazione  Selezionare la fonte in cui FGo! cercherà le informazioni sulla posizione
delle piste e dei parcheggi. Sono disponibili due opzioni:

Default - FGo! mostrerà i numeri delle piste basandosi sul file ~/.fgo/apt,
   generato a partire dal database FG_ROOT/Airports/apt.dat.gz. I numeri
   dei parcheggi verranno prelevati dalla directory FG_ROOT/Airports.

Scenery - FGo! mostrerà i numeri delle piste basandosi sul file ~/.fgo/apt,
   generato dal database FG_ROOT/Airports/apt.dat.gz, mentre i numeri
   dei parcheggi verranno prelevati dalla directory (o dalle directory, se
   sono stati definiti più percorsi) FG_SCENERY/Airports. 

In generale, con l'opzione "Default" FGo! mostra gli stessi dati usati
da FlightGear quando viene selezionata la località di partenza, mentre
scegliendo "Scenery" FGo! cerca i dati nella directory dello scenario.
In quest'ultimo caso si può aggiungere, nella finestra delle opzioni,
l'argomento '--prop:/sim/paths/use-custom-scenery-data=true', per far
sì che FlightGear utilizzi gli stessi dati.

Se non si è sicuri su quale opzione scegliere è preferibile attenersi alle
impostazioni predefinite. Impostare la dimensione di base del carattere, compresa nell'intervallo tra {0}
e {1}. Il valore speciale zero assegna al carattere una dimensione predefinita
dipendente dalla piattaforma. Impostazioni Mostra solo gli aeroporti installati Avvio di %s con le opzioni seguenti: Avvio di TerraSync con il comando seguente: Arresto di TerraSync Impostazioni di TerraSync Strumenti Traduzione italiana: Philip Lacroix Impossibile scaricare i dati. Impossibile avviare FlightGear! Aggiorna la lista degli aeroporti installati Directory di lavoro (facoltativa): [Avviso di FG0!] Non è stato possibile scaricare lo scenario. 