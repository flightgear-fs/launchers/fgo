��    O      �  k         �     �     �     �     �            	       #  z   +     �     �     �  �   �     �	     �	  T   �	      
  J   &
     q
     w
     �
     �
  3   �
  b   �
  l   +     �     �     �     �     �     �  
   �  5   �     )     .     6     ;     B  $   P     u     z  �   }     >  $   G  �   l       q   ,  j   �     	     )     /     ;     @     Y     _     f     k  
   w     �     �     �     �     �  %  �  �   �     o     x  #   �  *   �     �     �     
               6  !   P     r  1   �  �  �     Z  $   `     �     �  
   �  	   �     �    �  �   �  	     '   �  ,   �  �   �     �     �  x   �     f  ]   m  
   �     �  	   �     �  9   �  }   2  �   �     8     ?     U     [     b     w     �  7   �     �     �     �     �     �                #  �   &  
     Q     �   b     /   �   M   �   �   *   `!     �!     �!     �!     �!     �!  
   �!  	   �!     �!     �!     "     ("     1"     C"     R"  �  c"  u   �&     s'  $   �'  !   �'  (   �'     �'     (  	   (  8   $(  /   ](     �(      �(     �(  1   �(     *   !   	                      L              4   H               7                        9   B   A      K             
   %   <   8      1   N   =   '             5      J   $   E   D   "      2         ,             ?       C                      3       ;   F   -   G              0   O      (   .       &   I      @           )      /         +                        #      :   6   >   M    About Additional aircraft path: Airport data source: Airport database update: Airport: Authors: Automatic Automatic - FGo! will try to keep track of changes of
    FG_ROOT/Airports/apt.dat.gz file, and will rebuild its airport database
    when need it.

Manual - "Rebuild Airport Database" button needs to be used every time
    FG_ROOT/Airports/apt.dat.gz is changed. Build new airport database from current FG_ROOT/Airports/apt.dat.gz.
Useful in case when apt.dat.gz file has been updated. Cancel Cannot find apt.dat database. Cannot read apt.dat database. Cannot read version number of apt.dat database.

FGo! expects a proper header information at the beginning of apt.dat. For details about data format see http://data.x-plane.com/designers.html#Formats Carrier: Change language: Choose other language. If not selected,
FGo! will try to choose the system language. Clear Click here to download the METAR report
for selected (or nearest) airport. Close Config Files Decoded Default Directory where TerraSync saves downloaded scenery. Enter the path to "fgfs" file, or "run_fgfs.sh", if you are using
download_and_compile.sh scripts. Enter the path to "terrasync" file, or "run_terrasync.sh", if you are using
download_and_compile.sh scripts. Error Fetching report... File Find FlightGear is running... FlightGear settings Font size: Generating airport database,
this can take a while... Help License Load Manual Miscellaneous Modification of apt.dat.gz detected. None OK Optional parameter specifying FlightGear's working directory.
That is the directory where logs or screen-shots will be saved. If left blank,
the working directory is the user's home directory. Parking: Path to FlightGear's main directory. Path to additional aircraft directory. Multiple directories separated by
a colon can be included. Leave it empty if you are not using additional
aircraft directories. Path to executable file: Path to scenery. You can specify more than one path (separated by a colon)
in order from highest to lowest layer. Please make sure that paths: FG_BIN and FG_ROOT
in "Preferences" window are pointing to right directories. Port address used by TerraSync. Port: Preferences Quit Rebuild Airport Database Reset Run FG Rwy: Save & Quit Save as... Save settings Scenery Scenery Prefetch Scenery path: Select Scenario Select data source where FGo! will be looking for information about runways or
parking positions. There are two options:

Default - FGo! will show runway numbers based on ~/.fgo/apt file,
    which is generated from FG_ROOT/Airports/apt.dat.gz database.
    Parking names will be taken from FG_ROOT/AI/Airports directory.

Scenery - FGo! will show runway numbers based on ~/.fgo/apt file,
    which is generated from FG_ROOT/Airports/apt.dat.gz database.
    Parking names will be taken from FG_SCENERY/Airports folder
    - or folders - if multiple scenery paths are provided.

Generally speaking: "Default" option will tell FGo! to show the same
data that FlightGear uses by default when selecting starting location,
while "Scenery" option will tell FGo! to look for that data
directly in scenery folder. In that latter case, you may add
--prop:/sim/paths/use-custom-scenery-data=true argument into command
line options window to tell FlightGear to use the same data.

If not sure which option you should choose, the best is to stick
with the default setting. Set the base font size in the range from {0} to {1}. Zero is a special
value that sets font to a platform-dependent default size. Settings Show installed airports only Starting %s with following options: Starting TerraSync with following command: Stopping TerraSync TerraSync settings Tools Translation: Unable to download data. Unable to run FlightGear! Update list of installed airports Working directory (optional): [FGo! Warning] Scenery prefetch was unsuccessful. Project-Id-Version: PACKAGE VERSION
POT-Creation-Date: 2014-11-06 18:44+CET
PO-Revision-Date: 2013-07-24 12:51+0200
Last-Translator: chris_blues <chris@musicchris.de>
Language-Team: LANGUAGE <LL@li.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Virtaal 0.7.1
Generated-By: pygettext.py 1.5
 Über Zusätzliche Flugzeug-Verzeichnisse: Flughafen-Datenquelle: Flughafen Datenbank erneuern: Flughafen: Authoren: Automatisch Automatisch - FGo! wird versuchen Änderungen an FG_ROOT/Airports/apt.dat.gz
    zu erkennen, und falls nötig, die Flughafen Datenbank neu aufbauen.

Manuell - "Flughafen-Datenbank neu laden" Knopf muß jedes Mal gedrückt
    werden wenn FG_ROOT/Airports/apt.dat.gz verändert wurde. Baut eine neue Flughafen-Datenbank aus FG_ROOT/Airports/apt.dat.gz.
Das ist z.Bsp. sinnvoll wenn die Datei apt.dat.gz erneuert wurde. Abbrechen Apt.dat Datenbank wurde nicht gefunden. Apt.dat Datenbank kann nicht gelesen werden. Versionsnummer der apt.dat Datenbank kann nicht gelesen werden.

FGo! erwartet die richtige Kopf-Informationen am Anfang der apt.dat. Für weiterführende Informationen über das Datenformat siehe: http://data.x-plane.com/designers.html#Formats Träger: Sprache ändern: Wählen Sie eine andere Sprache. Falls keine Sprache
ausgewählt ist wird FGo! versuchen die System-Sprache zu erkennen. Leeren Hier klicken, um METAR Bericht für den
gewählten (oder nahesten) Flughafen herunterzuladen. Schließen Config Dateien Dekodiert Standard Verzeichnis, wohin TerraSync die Szenerien herunterlädt. Geben Sie den Ort an, wo sich die Datei "fgfs" oder,
falls Sie Download_und_Kompilier-Skripte nutzen, "run_fgfs.sh" befindet. Geben Sie den Ort an, wo sich die Datei "terrasync" oder,
falls Sie Download_und_Kompilier-Skripte nutzen, "run_terrasync.sh" befindet. Fehler Hole Wetterbericht... Datei Finden Flightgear läuft... FlightGear Einstellungen Schriftgröße Generiere Flughafen Datenbank,
das kann etwas dauern... Hilfe Lizenz Laden Manuell Verschiedenes apt.dat.gz wurde verändert. Keine OK Optionaler Parameter, der das Arbeitsverzeichnis von FlightGear benennt.
In diesem Verzeichnis werden Bildschirmfotos und Log-Dateien abgespeichert.
Bleibt dieses Feld leer, wird das Home-Verzeichnis des Benutzers benutzt. Parkplatz: Pfad zu FlightGear's Hauptverzeichnis. z.Bsp.:
/usr/share/games/FlightGear/fgdata Pfad zu zusätzlichen Flugzeug-Verzeichnissen. Mehrere Verzeichnisse können
mit Doppelpunkten getrennt angegeben werden. Lassen Sie es leer, falls Sie
keine zusätzlichen Flugzeug Verzeichnisse benutzen. Pfad zur ausführbaren Datei: Pfad zur Szenerie. Man kann mehrere Pfade angeben (mit Doppelpunkten getrennt).
In der Reihenfolge von der höchsten zur niedrigsten Priorität. Bitte stellen sie sicher, daß die Pfade FG_BIN und FG_ROOT
im "Eigenschaften"-Fenster auf die richtigen Verzeichnisse verweisen. Eine Port-Addresse, die TerraSync benutzt. Port: Eigenschaften Beenden Flughafen-Datenbank neu laden Zurücksetzen FG starten Rollbahn: Speichern & beenden Speichern unter ... Einstellungen speichern Szenerie Szenerie vorladen Szenerie Pfad: Szenario wählen Wählen Sie die Datenquelle, wo FGo! nach Informationen über Rollbahnen oder
Parkpositionen suchen soll. Es gibt zwei Möglichkeiten:

Standard - FGo! zeigt Rollbahnbezeichnungen aus der ~/.fgo/apt-
    Datei. Diese wird generiert aus der FG_ROOT/Airports/apt.dat.gz
    Datenbank. Parkplatznamen werden aus FG_ROOT/AI/Airports geholt.

Szenerie - FGo! zeigt Rollbahnbezeichnungen aus der ~/.fgo/apt-
    Datei. Diese wird generiert aus der FG_ROOT/Airports/apt.dat.gz
    Datenbank. Parkplatznamen werden aus FG_SCENERY/Airports bzw. den
    angegeben Szenerie-Verzeichnissen geholt.

Generell gesehen: Die Einstellung "Standard" veranlasst FGo! die
selben Daten zu zeigen, die FlightGear normalerweise benutzt, wenn man
die Startposition angibt, während die Option "Szenerie" FGo!
veranlasst, direkt im angegebenen Szenerie-Verzeichnis nachzusehen. Im
letzteren Fall könnten sie noch
--prop:/sim/paths/use-custom-scenery-data=true im Kommandozeilen-
fenster angeben, um FlightGear anzuzeigen, daß es die selben Daten
benutzen soll.

Sollten Sie sich unsicher sein, welche Option Sie benutzen sollten,
ist es wahrscheinlich am Besten, wenn Sie die Standard-Option wählen. Setzt die Schriftgröße im Bereich von {0} bis {1}. Null stellt die
Schrift auf die Standardgröße der Oberfläche. Einstellungen Nur installierte Flughäfen anzeigen Starte %s mit folgenden Optionen: Starte TerraSync mit folgendem Kommando: Stoppe TerraSync TerraSync Einstellungen Werkzeuge deutsche Übersetzung:
chris_blues <chris@musicchris.de> Die Daten können nicht heruntergeladen werden. Kann FlightGear nicht starten! Liste der Flughäfen auffrischen Arbeitsverzeichnis (optional): [FGo! Warnung] Szenerie Vorladen ist gescheitert. 