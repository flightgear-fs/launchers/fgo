Þ    O        k         ¸     ¹     ¿     Ù     î            	       #  z   +     ¦     ­     Ë  Ç   é     ±	     º	  T   Ë	      
  J   &
     q
     w
     
     
  3   
  b   È
  l   +               ±     ¶     »     Ô  
   è  5   ó     )     .     6     ;     B  $   P     u     z  À   }     >  $   G  ¦   l       q   ,  j        	     )     /     ;     @     Y     _     f     k  
   w                    ©     ·  %  Ç     í     o     x  #     *   ¹     ä     ÷     
               6  !   P     r  1     S  Â  !     %   8     ^  "   {          ¦     ®  7  µ     í       E     D   Õ         7     @     Q  	   ê     ô  	   v                  l   °        ª   ¾  	   i  *   s          «  &   ²     Ù     î  ^   ÿ  	   ^     h     x            ,        É     Ð    Ó  	   î   M   ø     F!     M"     m"  j   #  F   o#  
   ¶#     Á#     Î#  $   Õ#     ú#     $  
   $     $     1$     8$     H$  $   X$     }$     $  :  ­$  ½   è)     ¦*  -   ­*  7   Û*  F   +  *   Z+     +  	   +     £+  0   «+  %   Ü+  <   ,  %   ?,  M   e,     *   !   	                      L              4   H               7                        9   B   A      K             
   %   <   8      1   N   =   '             5      J   $   E   D   "      2         ,             ?       C                      3       ;   F   -   G              0   O      (   .       &   I      @           )      /         +                        #      :   6   >   M    About Additional aircraft path: Airport data source: Airport database update: Airport: Authors: Automatic Automatic - FGo! will try to keep track of changes of
    FG_ROOT/Airports/apt.dat.gz file, and will rebuild its airport database
    when need it.

Manual - "Rebuild Airport Database" button needs to be used every time
    FG_ROOT/Airports/apt.dat.gz is changed. Build new airport database from current FG_ROOT/Airports/apt.dat.gz.
Useful in case when apt.dat.gz file has been updated. Cancel Cannot find apt.dat database. Cannot read apt.dat database. Cannot read version number of apt.dat database.

FGo! expects a proper header information at the beginning of apt.dat. For details about data format see http://data.x-plane.com/designers.html#Formats Carrier: Change language: Choose other language. If not selected,
FGo! will try to choose the system language. Clear Click here to download the METAR report
for selected (or nearest) airport. Close Config Files Decoded Default Directory where TerraSync saves downloaded scenery. Enter the path to "fgfs" file, or "run_fgfs.sh", if you are using
download_and_compile.sh scripts. Enter the path to "terrasync" file, or "run_terrasync.sh", if you are using
download_and_compile.sh scripts. Error Fetching report... File Find FlightGear is running... FlightGear settings Font size: Generating airport database,
this can take a while... Help License Load Manual Miscellaneous Modification of apt.dat.gz detected. None OK Optional parameter specifying FlightGear's working directory.
That is the directory where logs or screen-shots will be saved. If left blank,
the working directory is the user's home directory. Parking: Path to FlightGear's main directory. Path to additional aircraft directory. Multiple directories separated by
a colon can be included. Leave it empty if you are not using additional
aircraft directories. Path to executable file: Path to scenery. You can specify more than one path (separated by a colon)
in order from highest to lowest layer. Please make sure that paths: FG_BIN and FG_ROOT
in "Preferences" window are pointing to right directories. Port address used by TerraSync. Port: Preferences Quit Rebuild Airport Database Reset Run FG Rwy: Save & Quit Save as... Save settings Scenery Scenery Prefetch Scenery path: Select Scenario Select data source where FGo! will be looking for information about runways or
parking positions. There are two options:

Default - FGo! will show runway numbers based on ~/.fgo/apt file,
    which is generated from FG_ROOT/Airports/apt.dat.gz database.
    Parking names will be taken from FG_ROOT/AI/Airports directory.

Scenery - FGo! will show runway numbers based on ~/.fgo/apt file,
    which is generated from FG_ROOT/Airports/apt.dat.gz database.
    Parking names will be taken from FG_SCENERY/Airports folder
    - or folders - if multiple scenery paths are provided.

Generally speaking: "Default" option will tell FGo! to show the same
data that FlightGear uses by default when selecting starting location,
while "Scenery" option will tell FGo! to look for that data
directly in scenery folder. In that latter case, you may add
--prop:/sim/paths/use-custom-scenery-data=true argument into command
line options window to tell FlightGear to use the same data.

If not sure which option you should choose, the best is to stick
with the default setting. Set the base font size in the range from {0} to {1}. Zero is a special
value that sets font to a platform-dependent default size. Settings Show installed airports only Starting %s with following options: Starting TerraSync with following command: Stopping TerraSync TerraSync settings Tools Translation: Unable to download data. Unable to run FlightGear! Update list of installed airports Working directory (optional): [FGo! Warning] Scenery prefetch was unsuccessful. Project-Id-Version: PACKAGE VERSION
POT-Creation-Date: 2014-11-06 18:44+CET
PO-Revision-Date: 2014-11-09 16:40+0900
Last-Translator: AOKI kiyohito <sambar.fgfs@gmail.com>
Language-Team: FlightGear.jpn.org
Language: ja
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Generated-By: pygettext.py 1.5
 ãã®ãã­ã°ã©ã ã«ã¤ãã¦ è¿½å ã®èªç©ºæ©ãã¼ã¿ã®ãã¹: ç©ºæ¸¯ãã¼ã¿ã®ã½ã¼ã¹: ç©ºæ¸¯ãã¼ã¿ãã¼ã¹ã®æ´æ°: ç©ºæ¸¯: ä½è: èªå èªå - FGo! ã¯ FG_ROOT/Airports/apt.dat.gz ãã¡ã¤ã«ã®å¤æ´ã®è¿½è·¡ãè©¦ã¿ãã¾ãå¿è¦ã«å¿ãç©ºæ¸¯ãã¼ã¿ãã¼ã¹ã®åæ§ç¯ãè©¦ã¿ã¾ãã

æå - FG_ROOT/Airports/apt.dat.gz ãå¤æ´ãããæ¯ã«ã"ç©ºæ¸¯ãã¼ã¿ãã¼ã¹ã®åæ§ç¯"  ãã¿ã³ãæ¼ãå¿è¦ãããã¾ãã ç¾å¨ã® FG_ROOT/Airports/apt.dat.gz ããæ°ããç©ºæ¸¯ãã¼ã¿ãã¼ã¹ãæ§ç¯ãã¾ãã
apt.dat.gzãææ°ã®å ´åã«æç¨ã§ãã ã­ã£ã³ã»ã« apt.dat ãã¼ã¿ãã¼ã¹ ãè¦ã¤ãããã¨ãã§ãã¾ããã apt.dat ãã¼ã¿ãã¼ã¹ãèª­ã¿è¾¼ããã¨ãã§ãã¾ããã apt.dat ãã¼ã¿ãã¼ã¹ã®ãã¼ã¸ã§ã³ãåã£ã¦ãã¾ããã

FGo!  ã¯ apt.dat ã®ãããã¼æå ±ãé©åã§ãããã¨ãæå¾ãã¦ãã¾ãããã¼ã¿ãã©ã¼ãããã®è©³ç´°ã«ã¤ãã¦ã¯http://data.x-plane.com/designers.html#Formatsãè¦ã¦ãã ããã ç©ºæ¯ : è¨èªãå¤æ´: è¨èªãé¸æãã¦ãã ãããé¸æããã¦ããªãå ´åãFGo! ã¯ã·ã¹ãã ã§è¨­å®ããã¦ããè¨èªãä½¿ããã¨ãè©¦ã¿ã¾ãã ã¯ãªã¢         ãããã¯ãªãã¯ããã¨ãé¸æãã(or è³è¿ã®)ç©ºæ¸¯ã®æ°è±¡éå ±ããã¦ã³ã­ã¼ããã¾ãã         éãã è¨­å®ãã¡ã¤ã« ãã³ã¼ã ããã©ã«ã TerraSyncããã¦ã³ã­ã¼ãããã·ã¼ããªã¼ãä¿å­ãããã©ã«ããæå®ãã¦ãã ããã "fgfs" ãã¡ã¤ã« (download_and_compile.shã¹ã¯ãªãããä½¿ç¨ãã¦ããå ´åã¯ "run_fgfs.sh" ãã¡ã¤ã«) ã¸ã®ãã¹ãå¥åãã¦ãã ããã "terrasync" ãã¡ã¤ã« (download_and_compile.shã¹ã¯ãªãããä½¿ç¨ãã¦ããå ´åã¯ "run_terrasync.sh" ãã¡ã¤ã«) ã¸ã®ãã¹ãå¥åãã¦ãã ããã ã¨ã©ã¼ æ°è±¡éå ±ãæé©åãã¦ãã¾ã... ãã¡ã¤ã« æ¢ã FlightGear ãå®è¡ãã¦ãã¾ãâ¦ FlightGear ã®è¨­å® æå­ãµã¤ãº: ç©ºæ¸¯ãã¼ã¿ãã¼ã¹ãçæãã¦ãã¾ãã
ãã°ãããå¾ã¡ãã ããã»ã»ã» ãã«ã ã©ã¤ã»ã³ã¹ èª­ã¿è¾¼ã¿ æå éå¤ãªè¨­å® apt.dat.gz ã®å¤æ´ãæ¤åºãã¾ããã ç¡ã OK FlightGearã®ä½æ¥­ãã£ã¬ã¯ããªãæå®ãããªãã·ã§ã³ã®ãã©ã¡ã¼ã¿ã§ãã
ããã¯ã­ã°ãã¹ã¯ãªã¼ã³ã·ã§ããã®ä¿å­åã«ãªãã¾ãã
ããç©ºæ¬ã®å ´åãã¦ã¼ã¶ã¼ã®ãã¼ã ãã£ã¬ã¯ããªãä½æ¥­ãã£ã¬ã¯ããªã«ãªãã¾ãã é§æ©å ´ FlightGear ã®ã¡ã¤ã³ãã©ã«ãã¸ã®ãã¹ãæå®ãã¦ãã ããã è¿½å ã®èªç©ºæ©ãã©ã«ãã¸ã®ãã¹ãå¥åãã¦ãã ãããåªååº¦ã®é«ãé ããã³ã­ã³(:)ã§åºåã£ã¦è¤æ°ã®ãã¹ãæå®ã§ãã¾ãã
è¿½å ã®èªç©ºæ©ãã©ã«ããä½¿ç¨ããªãå ´åãç©ºæ¬ã«ãã¦ããã¦ãã ããã å®è¡ãã¡ã¤ã«ã¸ã®ãã¹: ã·ã¼ããªã¼ã¸ã®ãã¹ãå¥åãã¦ãã ãããåªååº¦ã®é«ãé ããã³ã­ã³(:)ã§åºåã£ã¦è¤æ°ã®ãã¹ãæå®ã§ãã¾ãã "Prefernces"ã¦ã¤ã³ãã¦ãããFG_BIN ã¨ FG_ROOT ã®ãã¹ãæ­£ãããç¢ºèªãã¦ãã ããã TerraSync ãä½¿ç¨ãããã¼ãçªå·ãæå®ãã¦ãã ããã ãã¼ã: è©³ç´°è¨­å® çµäº ç©ºæ¸¯ãã¼ã¿ãã¼ã¹ãåæ§ç¯ ãªã»ãã FGãèµ·å æ»èµ°è·¯: ä¿å­ãã¦çµäº ä¿å­ è¨­å®ãä¿å­ ã·ã¼ããªã¼ ã·ã¼ããªã¼ã®ããªãã§ãã ã·ã¼ããªã¼ã®ãã¹: ã·ããªãªãé¸æ æ»èµ°è·¯ãé§æ©å ´ã«ã¤ãã¦FGo! ãåç§ãããã¼ã¿ã®ã½ã¼ã¹ãã©ããé¸æãã¦ãã ããã ããã«ã¯2ã¤ã®ãªãã·ã§ã³ãããã¾ã:

ããã©ã«ã - FGo! ã¯ FG_ROOT/Airports/apt.dat.gz ãã¼ã¿ãã¼ã¹ããçæãããã
    ~/.fgo/apt ãã¡ã¤ã«ãåã«æ»èµ°è·¯çªå·ãè¡¨ç¤ºãã¾ãã
    é§æ©å ´åã¯ FG_ROOT/AI/Airports ãã£ã¬ã¯ããªããå¾ã¾ãã

ã·ã¼ããªã¼ - FGo! ã¯ FG_ROOT/Airports/apt.dat.gz ãã¼ã¿ãã¼ã¹ããçæãããã
    ~/.fgo/apt ãã¡ã¤ã«ãåã«æ»èµ°è·¯çªå·ãè¡¨ç¤ºãã¾ãã
    é§æ©å ´åã¯ FG_SCENRY/Airports ãã©ã«ãããå¾ã¾ãã

ä¸è¬çãªäºé : éå§å°ç¹ã®é¸æã®ã¨ãã "ããã©ã«ã" ãªãã·ã§ã³ã§ã¯ FlightGear ãããã©ã«ãã§ä½¿ãã®ã¨åããã¼ã¿ãè¡¨ç¤ºãã¾ããã
 "ã·ã¼ããªã¼" ãªãã·ã§ã³ã§ã¯ã·ã¼ããªã¼ãã©ã«ãã®ä¸­ã®ãã¼ã¿ãç´æ¥è¦ãã§ãããã
å¾èã®ã±ã¼ã¹ã§ã¯ã FlightGear ã«åããã¼ã¿ãä½¿ãããæç¤ºããããã«ãä¸è¨ã®ã³ãã³ãã©ã¤ã³ãªãã·ã§ã³ãè¿½å ãããã¨ãã§ãã¾ãã
--prop:/sim/paths/use-custom-scenery-data=true 

ããªããã©ã®ãªãã·ã§ã³ãé¸æãã¹ããã¯ã£ããåãããªãå ´åã "ããã©ã«ã" ã»ããã£ã³ã°ãä½¿ç¨ãã¦ãã ããã åºæ¬çãªæå­ãµã¤ãºã {0} ãã {1} ã®éã§è¨­å®ãã¦ãã ããã 0ã¯æå­ãµã¤ãºããã©ãããã©ã¼ã ã§ã®ããã©ã«ããµã¤ãºã«ããç¹å¥ãªå¤ã§ãã è¨­å® ã¤ã³ã¹ãã¼ã«æ¸ã¿ã®ç©ºæ¸¯ã®ã¿è¡¨ç¤º %s ãä»¥ä¸ã®ãªãã·ã§ã³ä»ãã§éå§ãã¾ã: ä»¥ä¸ã®ã³ãã³ãã§ã·ã¼ããªã¼èªååå¾ãéå§ãã¾ã: ã·ã¼ããªã¼èªååå¾ãæ­¢ãã¾ã TerraSync ã®è¨­å® ãã¼ã« ç¿»è¨³: ãã¼ã¿ããã¦ã³ã­ã¼ãã§ãã¾ããã FlightGearãå®è¡ã§ãã¾ããï¼ ã¤ã³ã¹ãã¼ã«ããã¦ããç©ºæ¸¯ã®ãªã¹ããæ´æ° ä½æ¥­ãã©ã«ã (ãªãã·ã§ã³): [FGo! ã®è­¦å] ã·ã¼ããªã¼ã®ããªãã§ããã«å¤±æãã¾ããã 