��    O      �  k         �     �     �     �     �            	       #  z   +     �     �     �  �   �     �	     �	  T   �	      
  J   &
     q
     w
     �
     �
  3   �
  b   �
  l   +     �     �     �     �     �     �  
   �  5   �     )     .     6     ;     B  $   P     u     z  �   }     >  $   G  �   l       q   ,  j   �     	     )     /     ;     @     Y     _     f     k  
   w     �     �     �     �     �  %  �  �   �     o     x  #   �  *   �     �     �     
               6  !   P     r  1   �  �  �  	   F     P     o  1   �     �     �  
   �    �  �   �     |  /   �  *   �  �   �     �     �  T   �     (  `   0     �     �     �     �  >   �  e     o   r     �     �     �               -     C  A   Y     �     �     �     �  
   �  %   �     �     �  �   �     �  +   �  �   �     x  T   �  x   �     b      ~      �      �   (   �   	   �      �      �      �      �      �   	   !     !     0!     G!  �  X!  �   F&     �&     �&  *   '  1   9'     k'     }'     �'  %   �'  '   �'      �'  *   (  !   9(  <   [(     *   !   	                      L              4   H               7                        9   B   A      K             
   %   <   8      1   N   =   '             5      J   $   E   D   "      2         ,             ?       C                      3       ;   F   -   G              0   O      (   .       &   I      @           )      /         +                        #      :   6   >   M    About Additional aircraft path: Airport data source: Airport database update: Airport: Authors: Automatic Automatic - FGo! will try to keep track of changes of
    FG_ROOT/Airports/apt.dat.gz file, and will rebuild its airport database
    when need it.

Manual - "Rebuild Airport Database" button needs to be used every time
    FG_ROOT/Airports/apt.dat.gz is changed. Build new airport database from current FG_ROOT/Airports/apt.dat.gz.
Useful in case when apt.dat.gz file has been updated. Cancel Cannot find apt.dat database. Cannot read apt.dat database. Cannot read version number of apt.dat database.

FGo! expects a proper header information at the beginning of apt.dat. For details about data format see http://data.x-plane.com/designers.html#Formats Carrier: Change language: Choose other language. If not selected,
FGo! will try to choose the system language. Clear Click here to download the METAR report
for selected (or nearest) airport. Close Config Files Decoded Default Directory where TerraSync saves downloaded scenery. Enter the path to "fgfs" file, or "run_fgfs.sh", if you are using
download_and_compile.sh scripts. Enter the path to "terrasync" file, or "run_terrasync.sh", if you are using
download_and_compile.sh scripts. Error Fetching report... File Find FlightGear is running... FlightGear settings Font size: Generating airport database,
this can take a while... Help License Load Manual Miscellaneous Modification of apt.dat.gz detected. None OK Optional parameter specifying FlightGear's working directory.
That is the directory where logs or screen-shots will be saved. If left blank,
the working directory is the user's home directory. Parking: Path to FlightGear's main directory. Path to additional aircraft directory. Multiple directories separated by
a colon can be included. Leave it empty if you are not using additional
aircraft directories. Path to executable file: Path to scenery. You can specify more than one path (separated by a colon)
in order from highest to lowest layer. Please make sure that paths: FG_BIN and FG_ROOT
in "Preferences" window are pointing to right directories. Port address used by TerraSync. Port: Preferences Quit Rebuild Airport Database Reset Run FG Rwy: Save & Quit Save as... Save settings Scenery Scenery Prefetch Scenery path: Select Scenario Select data source where FGo! will be looking for information about runways or
parking positions. There are two options:

Default - FGo! will show runway numbers based on ~/.fgo/apt file,
    which is generated from FG_ROOT/Airports/apt.dat.gz database.
    Parking names will be taken from FG_ROOT/AI/Airports directory.

Scenery - FGo! will show runway numbers based on ~/.fgo/apt file,
    which is generated from FG_ROOT/Airports/apt.dat.gz database.
    Parking names will be taken from FG_SCENERY/Airports folder
    - or folders - if multiple scenery paths are provided.

Generally speaking: "Default" option will tell FGo! to show the same
data that FlightGear uses by default when selecting starting location,
while "Scenery" option will tell FGo! to look for that data
directly in scenery folder. In that latter case, you may add
--prop:/sim/paths/use-custom-scenery-data=true argument into command
line options window to tell FlightGear to use the same data.

If not sure which option you should choose, the best is to stick
with the default setting. Set the base font size in the range from {0} to {1}. Zero is a special
value that sets font to a platform-dependent default size. Settings Show installed airports only Starting %s with following options: Starting TerraSync with following command: Stopping TerraSync TerraSync settings Tools Translation: Unable to download data. Unable to run FlightGear! Update list of installed airports Working directory (optional): [FGo! Warning] Scenery prefetch was unsuccessful. Project-Id-Version: PACKAGE VERSION
POT-Creation-Date: 2014-11-06 18:44+CET
PO-Revision-Date: 2014-11-11 20:28+0200
Last-Translator: Robert 'erobo' Leda <erobo@wp.pl>
Language-Team: 
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Virtaal 0.7.1
Generated-By: pygettext.py 1.5
 Acerca de Ruta para aviones adicionales: Fuente de datos de aeropuertos: Actualizacion de la base de datos de aeropuertos: Aeropuerto: Autores: Automatico Automatico - FGo! reconstruye la base de datos de aeropuertos cuando ha
    sido detectado un cambio en FG_ROOT/Airports/apt.dat.gz.
Manual - El botón "Reconstruir Base de Datos de Aeropuertos" hay que
    pulsarlo cuando se produzca un cambio en FG_ROOT/Airports/apt.dat.gz. Genera una nueva base de datos de aeropuertos usando
FG_ROOT/Airports/apt.dat.gz. Útil si el fichero apt.dat.gz
ha sido actualizado. Cancelar No se puede encontrar la base de datos apt.dat. No se puede leer la base de datos apt.dat. No es posible leer el número de version de la base de datos apt.dat.
FGo! necesita un encabezamiento correcto al principio de apt.dat. Para
mas informaciones visita http://data.x-plane.com/designers.html#Formats Portaaviones: Cambiar Idioma: Elige otro idioma. Si no es seleccionado,
FGo! intentara usar el idioma del sistema. Limpiar Haz click aquí para descargar el reporte METAR
para el aeropuerto seleccionado (o mas cercano). Cerrar Ficheros de Configuración Descodificado Por Defecto Directorio donde TerraSync guardas los escenarios descargados. Introduce la ruta al fichero "fgfs" o "run_fgfs.sh"
si estas usando el script download_and_compile.sh Introduce la ruta al fichero "terrasync" o "run_terrasync.sh"
si estas usando el script download_and_compile.sh Error Obteniendo reporte... Archivo Buscar FlightGear esta ejecutandose... Ajustes de FlightGear Tamaño de la fuente: Generando base de datos de aeropuertos,
puede llevar un tiempo... Ayuda Licencia Cargar Manual Miscelaneo Modificacion detectada en apt.dat.gz. Ninguno OK Este es el directorio donde los logs y capturas de pantalla serán
guardados. Si se deja en blanco, la ruta de trabajo sera el directorio
home de tu usuario. Parking: Ruta al directorio principal de FlightGear. Ruta para el directorio de aeronaves adicional. Multiples directorios
separados por una coma pueden ser includidos. Dejalo vacio sino usas
directorios adicionales de aeronaves. Ruta al archivo ejecutable: Ruta para los escenarios. Puedes especificar mas de una ruta,
separado por una coma. Por favor, asegurate que las rutas: FG_BIN and FG_ROOT
en el submenu "Preferencias" apuntan a los directorios correctos. Puerto usado por TerraSync. Puerto: Preferencias Salir Reconstruir Base de Datos de Aeropuertos Reiniciar Ejecutar FG Pista: Guardar y Salir Guardar como... Guardar ajustes Escenario Predescargar Escenario Ruta a los escenarios: Elegir Escenario Selecciona el origen de datos donde FGo! buscara para encontrar la información acerca
de las pistas o las posiciones de parking. Ahí dos opciones:

Por Defecto - FGo! mostrara los números de las pistas basado en el fichero
        ~/.fgo/apt, que es generado a partir de la base de datos
        FG_ROOT/Airports/apt.dat.gz. Los nombres de Parking se extraerán del
        directorio FG_ROOT/AI/Airports.

Escenarios - FGo! mostrara los números de las pistas basado en el fichero
        ~/.fgo/apt, que es generado a partir de la base de datos
        FG_ROOT/Airports/apt.dat.gz. Los nombres de Parking se extraerán
        del directorio FG_SCENERY/Airports - o directorios - si varias
        rutas fueron configuradas.

En pocas palabras: La opción "Por Defecto" le indica a FGo! que debe mostrar
	 los mismos datos que FlightGear usa por defecto cuando se selecciona
	 la localización inicial, mientras que "Escenarios" le indica que debe
	 buscar esos datos en el directorio scenery. En este último caso, 
	 deberías añadir el argumento
	 --prop:/sim/paths/use-custom-scenery-data=true en la ventana de linea
	 de comandos para que FlightGear use los mismos datos.

Si no estas seguro de cual opción escoger, los mejor es dejarlo por defecto. Ajustar el tamaño base de la fuente en el rango de {0} a {1}. El cero
es un valor especial que define la fuente, que depende del valor por
defecto del sistema. Ajustes Mostrar aeropuertos instalados Ejecutando %s con las siguientes opciones: Ejecutando TerraSync con los siguientes comandos: Parando TerraSync Ajustes de TerraSync Herramientas Traducido por:
Canseco
Philip Lacroix No ha sido posible descargar los datos. No se puede ejecutar FlightGear! Actualizar lista de aeropuertos instalados Directorio de trabajo (opcional): [FGo! Warning] No ha sido posible predescargar el escenario. 