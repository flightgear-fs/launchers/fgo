��    O      �  k         �     �     �     �     �            	       #  z   +     �     �     �  �   �     �	     �	  T   �	      
  J   &
     q
     w
     �
     �
  3   �
  b   �
  l   +     �     �     �     �     �     �  
   �  5   �     )     .     6     ;     B  $   P     u     z  �   }     >  $   G  �   l       q   ,  j   �     	     )     /     ;     @     Y     _     f     k  
   w     �     �     �     �     �  %  �  �   �     o     x  #   �  *   �     �     �     
               6  !   P     r  1   �  u  �     8  -   D     r  4   �     �  	   �     �  P  �  �   :     �  2   �  /      �   P     .     =  d   Q     �  b   �     !     (     @     I  @   Q  p   �  y        }     �     �     �     �     �     �  Q   �     H     M     U     ]     d  &   k     �     �    �  	   �  3   �  �   �  $   �   �   �   t   \!  &   �!     �!     �!     "  /   "     E"  	   Q"     ["     c"     z"     �"     �"     �"     �"     �"    #  �   (     �(  -   �(  -   �(  3   )     S)     g)     })  #   �)  )   �)  $   �)  1   �)  $   )*  J   N*     *   !   	                      L              4   H               7                        9   B   A      K             
   %   <   8      1   N   =   '             5      J   $   E   D   "      2         ,             ?       C                      3       ;   F   -   G              0   O      (   .       &   I      @           )      /         +                        #      :   6   >   M    About Additional aircraft path: Airport data source: Airport database update: Airport: Authors: Automatic Automatic - FGo! will try to keep track of changes of
    FG_ROOT/Airports/apt.dat.gz file, and will rebuild its airport database
    when need it.

Manual - "Rebuild Airport Database" button needs to be used every time
    FG_ROOT/Airports/apt.dat.gz is changed. Build new airport database from current FG_ROOT/Airports/apt.dat.gz.
Useful in case when apt.dat.gz file has been updated. Cancel Cannot find apt.dat database. Cannot read apt.dat database. Cannot read version number of apt.dat database.

FGo! expects a proper header information at the beginning of apt.dat. For details about data format see http://data.x-plane.com/designers.html#Formats Carrier: Change language: Choose other language. If not selected,
FGo! will try to choose the system language. Clear Click here to download the METAR report
for selected (or nearest) airport. Close Config Files Decoded Default Directory where TerraSync saves downloaded scenery. Enter the path to "fgfs" file, or "run_fgfs.sh", if you are using
download_and_compile.sh scripts. Enter the path to "terrasync" file, or "run_terrasync.sh", if you are using
download_and_compile.sh scripts. Error Fetching report... File Find FlightGear is running... FlightGear settings Font size: Generating airport database,
this can take a while... Help License Load Manual Miscellaneous Modification of apt.dat.gz detected. None OK Optional parameter specifying FlightGear's working directory.
That is the directory where logs or screen-shots will be saved. If left blank,
the working directory is the user's home directory. Parking: Path to FlightGear's main directory. Path to additional aircraft directory. Multiple directories separated by
a colon can be included. Leave it empty if you are not using additional
aircraft directories. Path to executable file: Path to scenery. You can specify more than one path (separated by a colon)
in order from highest to lowest layer. Please make sure that paths: FG_BIN and FG_ROOT
in "Preferences" window are pointing to right directories. Port address used by TerraSync. Port: Preferences Quit Rebuild Airport Database Reset Run FG Rwy: Save & Quit Save as... Save settings Scenery Scenery Prefetch Scenery path: Select Scenario Select data source where FGo! will be looking for information about runways or
parking positions. There are two options:

Default - FGo! will show runway numbers based on ~/.fgo/apt file,
    which is generated from FG_ROOT/Airports/apt.dat.gz database.
    Parking names will be taken from FG_ROOT/AI/Airports directory.

Scenery - FGo! will show runway numbers based on ~/.fgo/apt file,
    which is generated from FG_ROOT/Airports/apt.dat.gz database.
    Parking names will be taken from FG_SCENERY/Airports folder
    - or folders - if multiple scenery paths are provided.

Generally speaking: "Default" option will tell FGo! to show the same
data that FlightGear uses by default when selecting starting location,
while "Scenery" option will tell FGo! to look for that data
directly in scenery folder. In that latter case, you may add
--prop:/sim/paths/use-custom-scenery-data=true argument into command
line options window to tell FlightGear to use the same data.

If not sure which option you should choose, the best is to stick
with the default setting. Set the base font size in the range from {0} to {1}. Zero is a special
value that sets font to a platform-dependent default size. Settings Show installed airports only Starting %s with following options: Starting TerraSync with following command: Stopping TerraSync TerraSync settings Tools Translation: Unable to download data. Unable to run FlightGear! Update list of installed airports Working directory (optional): [FGo! Warning] Scenery prefetch was unsuccessful. Project-Id-Version: 
POT-Creation-Date: 2014-11-06 18:44+CET
PO-Revision-Date: 2014-11-08 19:55+0100
Last-Translator: Olivier
Language-Team: French <kde-i18n-doc@kde.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Lokalize 1.5
Generated-By: pygettext.py 1.5
 A propos de Répertoire pour les aéronefs additionnels : Source des données aéroport : Mise à jour de la base de données des aéroports : Aéroport : Auteurs : Automatique Automatique - FGo! tentera de suivre les modifications du fichier
    FG_ROOT/Airports/apt.dat.gz et reconstruira la base de données des aéroports
    lorsque ce sera nécessaire.

Manuel - le bouton "Reconstruire la base de données des aéroports" doit être utilisé à chaque fois que
    FG_ROOT/Airports/apt.dat.gz est modifié. Construit une nouvelle base de données des aéroports à partir du fichier FG_ROOT/Airports/apt.dat.gz actuel.
Pratique losque le fichier apt.dat.gz a été mis à jour. Annuler Impossible de trouver la base de données apt.dat. Impossible de lire la base de données apt.dat. Impossible de lire le nombre de version de la base de données apt.dat.
FGo! s'attend de trouver un header correct au début de apt.dat. Pour desdétails sur les formats voir http://data.x-plane.com/designers.html#Formats Porte-avions : Changer la langue : Choisissez une autre langue. Si non sélectionné,
FGo! essaiera de détecter la langue du système. Effacer Cliquer ici pour télécharger le rapport METAR
pour l'aéroport sélectionné (ou le plus proche) Fermer Configurer les fichiers Décodé Défaut Répertoire où TerraSync sauvegarde les scènes téléchargées Entrez le chemin vers le fichier "fgfs", ou "run_fgfs.sh", si vous utilisez
les scripts download_and_compile.sh. Entrez le chemin vers le fichier "terrasync" ou "run_terrasync.sh", si vous utilisez
les scripts download_and_compile.sh. Erreur Récupération du rapport... Fichier Choisir FlightGear est lancé... Paramètres FlightGear Taille de la police: Génération de la base de données des aéroports,
ceci peut prendre du temps... Aide Licence Charger Manuel Divers Modification de apt.dat.gz détectée. Aucun Ok Paramètre optionnel précisant le répertoire de travail de FlightGear.
C'est le répertoire où les journaux ou les captures d'écrans seront sauvegardés. Si vous le laissez vide,
le répertoire de travail est le répertoire par défaut de l'utilisateur. Parking : Chemin vers le répertoire principal de FlightGear. Chemin vers des répertoires d'aéronefs additionnels. Plusieurs répertoires, séparés par :
peuvent être ajoutés. Laissez vide si vous n'utilisez pas de répertoires d'aéronefs additionnels. Chemin vers le fichier exécutable : Chemin vers les scènes. Vous pouvez spécifier plus d'un chemin (séparés par :)
dans l'ordre de la couche la plus haute à la couche la plus basse. Vérifiez que les chemins : FG_BIN and FG_ROOT
dans la fenêtre "Préférences" pointent vers les bons répertoires. Numéro de port utilisé par TerraSync Port : Préférences Quitter Reconstruire la base de données des aéroports Redémarrer Lancer FG Piste : Enregistrer et quitter Enregistrer sous Enregistrer les paramètres Scènes Téléchargement des scènes Chemin vers les scènes : Sélectionner un scénario Choisissez la source des données où FGo! cherchera les informations à propos des pistes ou
des positions de parking. Il y a deux options :

Défaut - FGo! affichera les numéros de pistes en se basant sur le fichier ~/.fgo/apt,
    qui est généré à partir de la base de données FG_ROOT/Airports/apt.dat.gz.
    Les noms de parking seront obtenus à partir du répertoire FG_ROOT/AI/Airports.

Scènes - FGo! affichera les numéros de pistes en se basant sur le fichier ~/.fgo/apt,
    qui est généré à partir de la base de données FG_ROOT/Airports/apt.dat.gz.
    Les noms de parking seront obtenus à partir du répertoire FG_SCENERY/Airports
    (ou des répertoires, si plusieurs chemins sont listés).

De manière générale, l'option "Défaut" dira à FGo! d'afficher la même information
que FlightGear utilise par défaut en sélectionnant l'emplacement de démarrage,
alors que l'option "Scènes" dira à FGo! de rechercher cette donnée directement
dans le répertoire des scènes. Dans ce dernier cas, vous pouvez ajouter
l'argument '--prop:/sim/paths/use-custom-scenery-data=true dans la fenêtre des options de ligne
de commande pour dire à FlightGear d'utiliser la même information.

Si vous ne savez pas quelle option choisir, le mieux est de conserver l'option par défaut. Fixe la taille de la police de base de {0} à {1}. Zéro est une valeur
spéciale qui fixe la police à la taille par défaut en fonction de la plate-forme. Paramètres Afficher uniquement les aéroports installés Démarrage de %s avec les options suivantes : Démarrage de TerraSync avec la commande suivante : Arrêt de TerraSync Paramètres TerraSync Outils Traduction : Olivier Faivre, f-ojac Impossible de télécharger les données. Impossible de démarrer FlightGear ! Mettre à jour la liste des aéroports installés Répertoire de travail (optionnel) : [Avertissement FGo!] Le téléchargement des scènes n'a pas fonctionné.' 