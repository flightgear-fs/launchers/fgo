��    *      l  ;   �      �     �     �     �     �     �     �     �     �     �                          %     -     2     @     E     H     Q  j   j     �     �     �     �                      
   #     .     <     D     R     b  #   k  *   �     �     �     �     �  h       �     �  
   �  	   �     �  
   �     �  	   �     �     �     �     �     	                    %     *     -     <  g   Z     �  
   �  	   �     �     �  
   	  
   	     	     ,	     ;	  	   P	     Z	     g	     z	  '   �	  0   �	     �	     �	     	
     %
     *             &           !   (         $                             )   "                                     '           %                                  
                      #      	               About Airport data source: Airport: Cancel Carrier: Change language: Close Default Error File Find FlightGear settings Help License Load Miscellaneous None OK Parking: Path to executable file: Please make sure that paths: FG_BIN and FG_ROOT
in "Preferences" window are pointing to right directories. Port: Preferences Quit Rebuild Airport Database Reset Run FG Rwy: Save & Quit Save as... Save settings Scenery Scenery path: Select Scenario Settings Starting %s with following options: Starting TerraSync with following command: Stopping TerraSync TerraSync settings Unable to run FlightGear! Working directory (optional): Project-Id-Version: PACKAGE VERSION
POT-Creation-Date: 2014-03-02 17:36+CET
PO-Revision-Date: 2010-05-23 23:35+0200
Last-Translator: snipey
Language-Team: 
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Virtaal 0.5.2
Generated-By: pygettext.py 1.5
 Over Vliegveld gegevens: Vliegveld: Annuleren Vliegdekschip: Kies taal: Sluiten Standaard Fout Bestand Bladeren FlightGear instellingen Help Licentie Openen Overig Geen OK Parkeerplaats: Pad naar uitvoerbaar bestand: Controleer AUB of de paden: FG_BIN en FG_ROOT
in het "Preferences" scherm naar de juiste mappen wijzen. Poort: Voorkeuren Afsluiten Maak Vliegveld Database Opnieuw Reset FG Starten Startbaan: Opslaan & Sluiten Opslaan als... Instellingen opslaan Landschap Scenery pad: Selecteer Scenario Instellingen %s wordt gestart met de volgende opties TerraSync wordt gestart met volgende commando's: TerraSync Stoppen TerraSync instellingen Kan FlightGear niet starten Actieve map (optioneel): 