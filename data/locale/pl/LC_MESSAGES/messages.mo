��    N      �  k   �      �     �     �     �     �     �        	   	      z        �     �     �  �   �     �	     �	  T   �	     
  J   
     a
     g
     t
     |
  3   �
  b   �
  l        �     �     �     �     �     �  
   �  5   �               &     +     2  $   @     e     j  �   m     .  $   7  �   \       q     j   �     �               +     0     I     O     V     [  
   g     r     �     �     �     �  %  �  �   �     _     h  #   �  *   �     �     �     �             !   3     U  1   s  �  �     r     ~     �     �  	   �     �     �  6  �  �   1     �  *   �  )     �   5  	   /     9  g   H  	   �  Q   �               )  	   5  A   ?  l   �  v   �     e     l     �     �  !   �     �     �  ;   �               $     ,     4  %   <     b     i  �   l     .  ,   7  �   d  !      �   4   b   �   '   ,!     T!     Z!     f!     o!     �!  
   �!     �!     �!     �!     �!     �!     �!     �!     "  z  "  �   �'  
   C(  #   N(  (   r(  -   �(     �(     �(  
   �(     �(  #   )  (   >)     g)  ;   �)     *   !   	                      K              4   H               7                        9   B   A      J             
   %   <   8      1   M   =   '             5          $   E   D   "      2         ,             ?       C                      3       ;   F   -   G              0   N      (   .       &   I      @           )      /         +                        #      :   6   >   L    About Additional aircraft path: Airport data source: Airport database update: Airport: Authors: Automatic Automatic - FGo! will try to keep track of changes of
    FG_ROOT/Airports/apt.dat.gz file, and will rebuild its airport database
    when need it.

Manual - "Rebuild Airport Database" button needs to be used every time
    FG_ROOT/Airports/apt.dat.gz is changed. Build new airport database from current FG_ROOT/Airports/apt.dat.gz.
Useful in case when apt.dat.gz file has been updated. Cancel Cannot find apt.dat database. Cannot read apt.dat database. Cannot read version number of apt.dat database.

FGo! expects a proper header information at the beginning of apt.dat. For details about data format see http://data.x-plane.com/designers.html#Formats Carrier: Change language: Choose other language. If not selected,
FGo! will try to choose the system language. Clear Click here to download the METAR report
for selected (or nearest) airport. Close Config Files Decoded Default Directory where TerraSync saves downloaded scenery. Enter the path to "fgfs" file, or "run_fgfs.sh", if you are using
download_and_compile.sh scripts. Enter the path to "terrasync" file, or "run_terrasync.sh", if you are using
download_and_compile.sh scripts. Error Fetching report... File Find FlightGear is running... FlightGear settings Font size: Generating airport database,
this can take a while... Help License Load Manual Miscellaneous Modification of apt.dat.gz detected. None OK Optional parameter specifying FlightGear's working directory.
That is the directory where logs or screen-shots will be saved. If left blank,
the working directory is the user's home directory. Parking: Path to FlightGear's main directory. Path to additional aircraft directory. Multiple directories separated by
a colon can be included. Leave it empty if you are not using additional
aircraft directories. Path to executable file: Path to scenery. You can specify more than one path (separated by a colon)
in order from highest to lowest layer. Please make sure that paths: FG_BIN and FG_ROOT
in "Preferences" window are pointing to right directories. Port address used by TerraSync. Port: Preferences Quit Rebuild Airport Database Reset Run FG Rwy: Save & Quit Save as... Save settings Scenery Scenery Prefetch Scenery path: Select Scenario Select data source where FGo! will be looking for information about runways or
parking positions. There are two options:

Default - FGo! will show runway numbers based on ~/.fgo/apt file,
    which is generated from FG_ROOT/Airports/apt.dat.gz database.
    Parking names will be taken from FG_ROOT/AI/Airports directory.

Scenery - FGo! will show runway numbers based on ~/.fgo/apt file,
    which is generated from FG_ROOT/Airports/apt.dat.gz database.
    Parking names will be taken from FG_SCENERY/Airports folder
    - or folders - if multiple scenery paths are provided.

Generally speaking: "Default" option will tell FGo! to show the same
data that FlightGear uses by default when selecting starting location,
while "Scenery" option will tell FGo! to look for that data
directly in scenery folder. In that latter case, you may add
--prop:/sim/paths/use-custom-scenery-data=true argument into command
line options window to tell FlightGear to use the same data.

If not sure which option you should choose, the best is to stick
with the default setting. Set the base font size in the range from {0} to {1}. Zero is a special
value that sets font to a platform-dependent default size. Settings Show installed airports only Starting %s with following options: Starting TerraSync with following command: Stopping TerraSync TerraSync settings Tools Unable to download data. Unable to run FlightGear! Update list of installed airports Working directory (optional): [FGo! Warning] Scenery prefetch was unsuccessful. Project-Id-Version: FGo! 1.5.4
POT-Creation-Date: 2014-11-06 18:44+CET
PO-Revision-Date: 2014-11-07 13:17+0100
Last-Translator: Robert 'erobo' Leda <erobo@wp.pl>
Language-Team: LANGUAGE <LL@li.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 1.6.10
Generated-By: pygettext.py 1.5
 O programie Dodadkowy katalog z samolotami: Źródło danych o lotniskach: Aktualizowanie bazy lotnisk: Lotnisko: Autorzy: Automatyczne Automatyczne - Program będzie starał się śledzić zmiany w pliku
    FG_ROOT/Airports/apt.dat.gz file i przebuduje własną bazę lotnisk
    gdy będzie to konieczne.

Ręczne - Trzeba użyć przycisku "Przebuduj Bazę Lotnisk" za każdym razem,
    gdy plik FG_ROOT/Airports/apt.dat.gz ulega modyfikacji. Zbuduj nową bazę lotnisk na podstawie danych z pliku
FG_ROOT/Airports/apt.dat.gz. Przycisk ten powinien zostać użyty
jeśli baza danych apt.dat.gz uległa zmianie. Anuluj Nie można odnaleźć bazy danych apt.dat. Nie można odczytać bazy danych apt.dat. Nie można odczytać numeru wersji bazy danych apt.dat.

FGo! oczekuje poprawnego nagłówka na początku bazy apt.dat. Szczegółowe informacje na temat formatu bazy danych można znaleźć pod adresem http://data.x-plane.com/designers.html#Formats Lot-wiec: Zmień język: Wybierz inny język. Jeżeli język nie został wybrany,
program postara się wybrać język systemowy. Wyczyść Kliknij tu, aby pobrać raport pogodowy
z wybranego (lub najbliższego) lotniska. Zamknij Pliki Konfiguracyjne Rozkodowany Domyślny Katalog, w którym program TerraSync zapisuje pobraną scenerię. Podaj ścieżkę do pliku "fgfs" lub do pliku "run_fgfs.sh"
jeśli używasz skryptu download_and_compile.sh. Podaj ścieżkę do pliku "terrasync" lub do pliku "run_terrasync.sh"
jeśli używasz skryptu download_and_compile.sh. Błąd Pobieranie raportu... Plik Znajdź FlightGear został uruchomiony... Ustawienia FlightGear Rozmiar czcionki: Trwa generowanie bazy lotnisk,
może to chwilę potrwać... Pomoc Licencja Wczytaj Ręczne Różne Wykryto modyfikację pliku apt.dat.gz Żaden OK Opcjonalny parametr określający katalog roboczy FlightGear.
To tam będą zapisywane logi czy screenshoty. Jeżeli pozostawiony pusty,
katalogiem roboczym będzie katalog domowy użytkownika. Parking: Ścieżka do katalogu głównego FlightGear. Ścieżka do dodatkowego katalogu z samolotami. Można podać wiele,
rozdzielonych dwukropkiem ścieżek. Pozostaw to pole puste jeżeli
nie używasz dodatkowych katalogów. Ścieżka do pliku wykonywalnego: Ścieżka do scenerii. Możesz podać więcej niż jedną ścieżkę (rozdzieloną dwukropkiem)
w kolejności od najwyższej do najniższej warstwy. Upewnij się, że ścieżki: FG_BIN i FG_ROOT
w oknie "Preferencje" wskazują właściwe katalogi. Numer portu używanego przez TerraSync. Port: Preferencje Zakończ Przebuduj Bazę Lotnisk Zresetuj Uruchom FG Pas: Zapisz i Wyjdź Zapisz jako... Zapisz ustawienia Sceneria Pobierz Scenerię Ścieżka do scenerii: Wybierz Scenariusz Wybierz źródło danych, z którego program będzie czerpał dane o pasach startowych
i pozycjach parkingowych. Są dwie opcje:

Domyślne - FGo! będzie pokazywać dane o pasach startowych w oparciu o
    informacje zgromadzone w pliku ~/.fgo/apt, który został
    wygenerowany na podstawie informacji pochodzących z pliku
    FG_ROOT/Airports/apt.dat.gz.

    Informacje o parkingach zostaną pobrane z katalogu:
    FG_ROOT/AI/Airports.

Sceneria - FGo! będzie pokazywać dane o pasach startowych w oparciu o
    informacje zgromadzone w pliku ~/.fgo/apt, który został
    wygenerowany na podstawie informacji pochodzących z pliku
    FG_ROOT/Airports/apt.dat.gz.

    Informacje o parkingach, zostaną pobrane z katalogu:
    FG_SCENERY/Airports - lub katalogów, jeśli zostały podane ścieżki
    do wielu scenerii.

Podsumowując...
Gdy zostanie wybrana opcja "Domyślne", program będzie korzystał z
tych samych danych, z których domyślnie korzysta FlightGear.
Tymczasem, gdy zostanie wybrana opcja "Sceneria", FGo! będzie szukało
tych informacji bezpośrednio w scenerii. W tym ostatnim przypadku,
można w oknie linii poleceń dodać opcję:
--prop:/sim/paths/use-custom-scenery-data=true, aby upewnić się, że
FlightGear także będzie pobierał informacje bezpośrednio ze scenerii.

Jeśli nie jesteś pewny którą opcję wybrać, najlepiej pozostaw
ustawienia domyślne. Ustaw główną wielkość czcionki w przedziale od {0} do {1}. Zero jest specjalną wartością,
która ustawia wielkość czcionki na wartość domyślną dla danej platformy. Ustawienia Pokaż tylko zainstalowane lotniska Uruchamiam %s z następującymi opcjami: Uruchamiam TerraSync następującą komendą: Zatrzymuję TerraSync Ustawienia TerraSync Narzędzia Nie można pobrać danych. Nie można uruchomić FlightGear'a! Odśwież listę zainstalowanych lotnisk Katalog roboczy (opcjonalnie): [Ostrzeżenie FGo!] Pobieranie scenerii nie powiodło się. 